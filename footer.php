  <footer id="footer">

    <div class="content">

      <div class="col">

        <p class="logo"><a href="<?php get_bloginfo("url"); ?>">Logo</a></p>

        <p><?php 
          if(pll_current_language() == 'es') {
              echo 'Promoviendo la salud y la alimentación sana, sustentable e inclusiva en Latinoamérica y en el Caribe';
          } else if(pll_current_language() == 'pt') {
              echo ''; 
          }  
        ?></p>

        <div class="redes">

          <a href="https://twitter.com/ColansaLAC" target="_blank"><i class="fab fa-twitter"></i></a>

          <a href="https://www.youtube.com/channel/UCJIgx7-bRRUTrgGavtesw6w" target="_blank"><i class="fab fa-youtube"></i></a>

        </div>

      </div>

      <div class="col">
      
       <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

      </div>

      <div class="col">
      
        <p class="footitle"><strong><?php 
          if(pll_current_language() == 'es') {
              echo 'Contacto';
          } else if(pll_current_language() == 'pt') {
              echo 'Contato'; 
          }  
        ?></strong></p>

        <p>contacto@colansa.org</p>

      </div>

    </div>

  </footer>

<?php wp_footer(); ?>

    <script src="https://kit.fontawesome.com/04aeb4c834.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" src="https://use.fontawesome.com/releases/v5.4.1/css/all.css">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        
        $('.partnerlist').slick({
          arrows: false,
          autoplay: true,
          dots: true,
          slidesToShow: 4,
          slidesToScroll: 4,
          autoplaySpeed: 4000,
          responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      
      });
    </script>

  </body>
</html>