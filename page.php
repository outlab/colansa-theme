<?php get_header() ?>

<div class="internas basic">

    <?php while ( have_posts() ) : the_post(); ?>

        <div class="content">

            <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

            <h1 class="pagetitle"><?php the_field('title_page'); ?></h1>

            <div class="subtitle"><?php the_field('sub_text_page'); ?></div>

            <div class="clear"></div>

            <div class="maincontent">


            <div id="conteudo"></div>

                <?php the_content(); ?>

            </div>

        </div>

    <?php endwhile;
        wp_reset_query();
    ?>

    <?php if ( is_page( array( 'sobre', 'quem-somos' ) ) ) : ?>
        <?php get_template_part( 'components/partner-section' ) ?>
    <?php endif; ?>

    <?php if ( is_page( array( 'biblioteca', 'biblioteca-pt-br', 'library' ) ) ) : ?>
        <div class="content">
            <?php get_template_part( 'components/library-section' ) ?>
        </div>
    <?php endif; ?>

    <?php if ( is_page( array( 'eventos', 'eventos-pt-br', 'events' ) ) ) : ?>
        <div class="content">
            <?php get_template_part( 'components/events-section' ) ?>
        </div>
    <?php endif; ?>

    <div class="clear"></div>

</div>

<?php get_footer() ?>