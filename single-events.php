<?php get_header() ?>

<div class="internas content">

<div id="conteudo"></div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="singlevents">

          <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

          <h1 class="pagetitle"><?php the_title(); ?></h1>

          <p class="data"><?php the_field('data_evento'); ?> - <?php the_field('data_evento_final'); ?> | <?php
            $categories = get_the_terms( $post->ID , 'category_events' );
            $i = 1;
            foreach ( $categories as $category ) {
              echo $category->name;
              echo ($i < count($categories))? " / " : "";
              $i++;
            }
          ?></p>

          <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>" class="destacada">

          <div class="text">

            <?php the_content(); ?>

            <?php if (get_field('tipo_insc_evento') == 'link'): ?>
              <a href="<?php the_field('link_insc_evento'); ?>" target="_blank" class="btn green"><?php the_field('btn_insc_evento'); ?></a>
            <?php endif; ?>
            <?php if (get_field('tipo_insc_evento') == 'form'): ?>

              <button class="btn green openinsc"><?php 
              if(pll_current_language() == 'es') {
                  echo 'Inscríbete';
              } else if(pll_current_language() == 'pt') {
                  echo 'Inscreva-se'; 
              }  
              ?></button>

            <?php endif; ?>

            <?php 
              $event_start_date = get_field('data_completa_evento');
              $event_print_date = date('d/m/Y',strtotime($event_start_date));
              $event_start_time = get_field('data_completa_evento');
              $event_end_date = get_field('data_fim_evento');
              $event_print_enddate = date('d/m/Y',strtotime($event_end_date));
              $event_end_time = get_field('data_fim_evento');

              $calendarDate = date('Ymd',strtotime($event_start_date)) .'T'. date('Hi',strtotime($event_start_time)). '00/' . date('Ymd',strtotime($event_end_date)) .'T'. date('Hi',strtotime($event_end_date)) . '00';
            ?>

            <a href="https://www.google.com/calendar/render?action=TEMPLATE&dates=<?php echo $calendarDate; ?>&text=<?php the_title(); ?>&sf=true&output=xml" class="btn bordergreen" target="_blank"><?php 
            if(pll_current_language() == 'es') {
                echo 'Añádelo en Google Calendar';
            } else if(pll_current_language() == 'pt') {
                echo 'Google Calendar'; 
            }  
            ?></a>

          </div>

          <div class="clear"></div>

          <div class="insc">

            <h3 class="sectitle"><?php 
            if(pll_current_language() == 'es') {
                echo 'Inscríbete en el evento';
            } else if(pll_current_language() == 'pt') {
                echo 'Preencha seus dados para se inscrever no evento'; 
            }  
            ?></h3>

            <?php if(pll_current_language() == 'es') {
                echo do_shortcode('[contact-form-7 id="80" title="Eventos - ES"]');
            } else if(pll_current_language() == 'pt') {
              echo do_shortcode('[contact-form-7 id="11" title="Eventos - PT"]');
            } ?>

          </div>

    </div>

    <?php endwhile; else: ?>
      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

  <div class="clear"></div>

  <?php get_template_part( 'components/newsletter' ) ?>

</div>

<?php get_footer() ?>