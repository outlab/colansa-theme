<?php /* Template Name: Notícias */ ?>

<?php get_header() ?>

<div class="internas">

  <div class="content">

      <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

      <h1 class="sectitle"><?php the_title(''); ?></h1>

      <?php the_content(); ?>

  </div>

  <div class="content">

  <div id="conteudo"></div>

    <div class="blogposts">

      <?php

        $postsdestaque_query_args = array(
          'post_type' => 'post', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date'
        );

        $postsdestaque_query = new WP_Query( $postsdestaque_query_args );

        if ( $postsdestaque_query->have_posts() ) : ?>

        <?php while( $postsdestaque_query->have_posts() ) : $postsdestaque_query->the_post(); ?>

        <div class="boxpost">

          <a href="<?php the_permalink(); ?>"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>"></a>

          <p class="cat"><?php $c = get_the_category(); echo $c[0]->cat_name; ?></p>

          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

          <a href="<?php the_permalink(); ?>" class="continue"><?php 
            if(pll_current_language() == 'es') {
                echo 'Lee mas';
            } else if(pll_current_language() == 'pt') {
                echo 'Leia mais'; 
            } else if (pll_current_language() == 'en') {
              echo 'Read more';
            }
          ?> <i class="fas fa-long-arrow-alt-right"></i></a>

          <div class="clear"></div>

        </div>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum post nessa seção.').'</p>'; endif; ?>  

    </div>

    <div class="sideblog">

      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Blog") ) : ?><?php endif;?>

    </div>

  <div class="clear"></div>

  </div>

</div>

<?php get_footer() ?>