<?php

add_action( 'init', 'create_library_cpt' );
function create_library_cpt() {

  register_post_type( 'library',

    array(
      'labels' => array(
        'name'                  => _x( 'Biblioteca', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Publicação', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Biblioteca', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Publicação', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Adicionar Nova', 'textdomain' ),
        'add_new_item'          => __( 'Adicionar Nova Publicação', 'textdomain' ),
        'new_item'              => __( 'Nova Publicação', 'textdomain' ),
        'edit_item'             => __( 'Editar Publicação', 'textdomain' ),
        'view_item'             => __( 'Ver Publicação', 'textdomain' ),
        'all_items'             => __( 'Todas as Publicações', 'textdomain' ),
        'search_items'          => __( 'Buscar na Biblioteca', 'textdomain' ),
        'parent_item_colon'     => __( 'Biblioteca relacionada:', 'textdomain' ),
        'not_found'             => __( 'Nenhuma Publicação encontrada.', 'textdomain' ),
        'not_found_in_trash'    => __( 'Nenhuma Publicação encontrada na lixeira.', 'textdomain' ),
        'featured_image'        => _x( 'Imagem de Fundo da Publicação', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Incluir Imagem', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remover Imagem', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Usar como Imagem', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Arquivo da Biblioteca', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Inserir na Publicação', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Incluido nessa Publicação', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filtrar lista da Biblioteca', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Navegue pela lista da Biblioteca', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Lista da Biblioteca', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
      ),

      'public' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'taxonomies' ),
      'menu_icon' => 'dashicons-portfolio',
      'show_in_rest' => true,
    )
  );

};

  $labeltipo = array(
    'name'              => _x( 'Tipo', 'taxonomy general name', 'textdomain' ),
    'singular_name'     => _x( 'Tipo', 'taxonomy singular name', 'textdomain' ),
    'search_items'      => __( 'Buscar Tipo', 'textdomain' ),
    'all_items'         => __( 'Todas os Tipos', 'textdomain' ),
    'parent_item'       => __( 'Tipo', 'textdomain' ),
    'parent_item_colon' => __( 'Tipo:', 'textdomain' ),
    'edit_item'         => __( 'Editar Tipo', 'textdomain' ),
    'update_item'       => __( 'Atualizar Tipo', 'textdomain' ),
    'add_new_item'      => __( 'Adicionar novo Tipo', 'textdomain' ),
    'new_item_name'     => __( 'Novo Tipo', 'textdomain' ),
    'menu_name'         => __( 'Tipo', 'textdomain' ),
  );

  $labelcategory = array(
    'name'              => _x( 'Categoria', 'taxonomy general name', 'textdomain' ),
    'singular_name'     => _x( 'Categoria', 'taxonomy singular name', 'textdomain' ),
    'search_items'      => __( 'Buscar Categoria', 'textdomain' ),
    'all_items'         => __( 'Todas as Categorias', 'textdomain' ),
    'parent_item'       => __( 'Categoria', 'textdomain' ),
    'parent_item_colon' => __( 'Categoria:', 'textdomain' ),
    'edit_item'         => __( 'Editar Categoria', 'textdomain' ),
    'update_item'       => __( 'Atualizar Categoria', 'textdomain' ),
    'add_new_item'      => __( 'Adicionar Nova Categoria', 'textdomain' ),
    'new_item_name'     => __( 'Nova Categoria', 'textdomain' ),
    'menu_name'         => __( 'Categoria', 'textdomain' ),
  );

  register_taxonomy(  
      'category_library', 
      'library',
      array(  
          'hierarchical' => true,  
          'labels' => $labelcategory,
          'query_var' => true,
          'show_in_rest' => true,
          'rewrite' => array(
              'slug' => 'categoria',
              'with_front' => false
          )
      )  
  );
  register_taxonomy(  
    'type_library', 
    'library',
    array(  
        'hierarchical' => true,  
        'labels' => $labeltipo,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array(
            'slug' => 'tipo',
            'with_front' => false
        )
    )  
  ); 

?>