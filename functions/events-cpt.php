<?php

add_action( 'init', 'create_events_cpt' );
function create_events_cpt() {

  register_post_type( 'events',

    array(
      'labels' => array(
        'name'                  => _x( 'Eventos', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Evento', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Eventos', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Evento', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Adicionar Novo', 'textdomain' ),
        'add_new_item'          => __( 'Adicionar Novo Evento', 'textdomain' ),
        'new_item'              => __( 'Novo Evento', 'textdomain' ),
        'edit_item'             => __( 'Editar Evento', 'textdomain' ),
        'view_item'             => __( 'Ver Evento', 'textdomain' ),
        'all_items'             => __( 'Todos os Eventos', 'textdomain' ),
        'search_items'          => __( 'Buscar Eventos', 'textdomain' ),
        'parent_item_colon'     => __( 'Eventos relacionados:', 'textdomain' ),
        'not_found'             => __( 'Nenhum Evento encontrado.', 'textdomain' ),
        'not_found_in_trash'    => __( 'Nenhum Evento encontrado na lixeira.', 'textdomain' ),
        'featured_image'        => _x( 'Imagem de Fundo do Evento', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Incluir Imagem', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remover Imagem', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Usar como Imagem', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Arquivo do Eventos', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Inserir no Evento', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Incluido nesse Evento', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filtrar lista de Eventos', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Navegue pela lista de Eventos', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Lista de Eventos', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
      ),

      'public' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'taxonomies' ),
      'menu_icon' => 'dashicons-calendar-alt',
      'show_in_rest' => true,
    )
  );

};

  $labelcategory = array(
    'name'              => _x( 'Categoria', 'taxonomy general name', 'textdomain' ),
    'singular_name'     => _x( 'Categoria', 'taxonomy singular name', 'textdomain' ),
    'search_items'      => __( 'Buscar Categoria', 'textdomain' ),
    'all_items'         => __( 'Todas as Categorias', 'textdomain' ),
    'parent_item'       => __( 'Categoria', 'textdomain' ),
    'parent_item_colon' => __( 'Categoria:', 'textdomain' ),
    'edit_item'         => __( 'Editar Categoria', 'textdomain' ),
    'update_item'       => __( 'Atualizar Categoria', 'textdomain' ),
    'add_new_item'      => __( 'Adicionar Nova Categoria', 'textdomain' ),
    'new_item_name'     => __( 'Nova Categoria', 'textdomain' ),
    'menu_name'         => __( 'Categoria', 'textdomain' ),
  );

  register_taxonomy(  
      'category_events', 
      'events',
      array(  
          'hierarchical' => true,  
          'labels' => $labelcategory,
          'query_var' => true,
          'show_in_rest' => true,
          'rewrite' => array(
              'slug' => 'categoria-evento',
              'with_front' => false
          )
      )  
  );

?>