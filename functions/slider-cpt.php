<?php

add_action( 'init', 'create_slider_cpt' );
function create_slider_cpt() {

  register_post_type( 'slider',

    array(
      'labels' => array(
        'name'                  => _x( 'Slides', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Slide', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Slides', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Slide', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Adicionar Novo', 'textdomain' ),
        'add_new_item'          => __( 'Adicionar Novo Slide', 'textdomain' ),
        'new_item'              => __( 'Novo Slide', 'textdomain' ),
        'edit_item'             => __( 'Editar Slide', 'textdomain' ),
        'view_item'             => __( 'Ver Slide', 'textdomain' ),
        'all_items'             => __( 'Todos as Slides', 'textdomain' ),
        'search_items'          => __( 'Buscar Slides', 'textdomain' ),
        'parent_item_colon'     => __( 'Slides relacionados:', 'textdomain' ),
        'not_found'             => __( 'Nenhum Slide encontrado.', 'textdomain' ),
        'not_found_in_trash'    => __( 'Nenhum Slide encontrado na lixeira.', 'textdomain' ),
        'featured_image'        => _x( 'Imagem de Fundo do Slide', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Incluir Imagem', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remover Imagem', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Usar como Imagem', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Arquivo de Slides', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Inserir em Slide', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Incluido nesse Slide', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filtrar lista de Slides', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Navegue pela lista de Slides', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Lista de Slides', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
      ),

      'public' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'taxonomies' ),
      'menu_icon' => 'dashicons-format-image',
      'show_in_rest' => true,
    )
  );

};

?>