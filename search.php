<?php get_header() ?>

<div class="internas">

  <div class="content">

    <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

    <h2 class="sectitle">Você buscou por: <?php printf( __( '%s' ), get_search_query() ); ?></h2>

    <div class="listaposts">

      <?php

      $postsearch_query_args = array(
        'post_type' => 'post', 
        //'posts_per_page' => -1,
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'date',
        's' =>$s
      );

      $postsearch_query = new WP_Query( $postsearch_query_args );

      if ( $postsearch_query->have_posts() ) : ?>

      <?php while( $postsearch_query->have_posts() ) : $postsearch_query->the_post(); ?>

        <div class="boxpost">

            <a href="<?php the_permalink(); ?>"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>"></a>

            <div class="text">

              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

              <?php the_excerpt(); ?>

              <a href="<?php the_permalink(); ?>" class="continue">leia mais</a>

              <div class="clear"></div>

            </div>

            <div class="clear"></div>

        </div>

      <?php endwhile; ?>

    <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum post nessa seção.').'</p>'; endif; ?>    

  </div>

  <div class="clear"></div>

  </div>

</div>

<?php get_footer() ?>