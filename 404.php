<?php get_header() ?>

<div class="internas fourofour">

  <div class="content">

  <div id="conteudo"></div>

      <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

      <img src="<?php bloginfo('template_directory');?>/assets/images/404.png" alt="Erro 404" />      

      <div class="text">

        <h1>Erro 404</h1>

        <a href="<?php bloginfo('url'); ?>" class="btn bordergreen">Voltar a home</a>

      </div>

  </div>

  <div class="clear"></div>

</div>

<?php get_footer() ?>