<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

<div class="homepage">



  <section class="slider">

    <?php

    $slider_query_args = array(
      'post_type' => 'slider', 
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'order' => 'DESC',
      'orderby' => 'date',
    );

    $slider_query = new WP_Query( $slider_query_args );

    if ( $slider_query->have_posts() ) : ?>

    <?php while( $slider_query->have_posts() ) : $slider_query->the_post(); ?>

    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

      <div class="slide" style="background: url(<?php echo $image[0]; ?>) no-repeat center center">

        <div class="text">
        
          <h2><?php the_title(''); ?></h2>

          <?php the_content(''); ?>

        </div>

      </div>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum post nessa seção.').'</p>'; endif; ?>

  </section>

  <?php 
    $args = array(
      'pagename' => 'home', 
    );
    $the_query = new WP_Query( $args ); 
  ?>
  <?php if ( $the_query->have_posts() ) : ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

  <section class="abouthome">

  <div id="conteudo"></div>
  
    <div class="content">

      <?php if( have_rows('sobre_section') ): ?>
      <?php while( have_rows('sobre_section') ): the_row(); 

          $title = get_sub_field('titulo_sobre');
          $text = get_sub_field('texto_sobre');
          $button = get_sub_field('botao_sobre');
          $link = get_sub_field('link_sobre');
          $image = get_sub_field('imagem_sobre');

      ?>

        <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" />
    
        <div class="text">
        
          <h2 class="title"><?php echo $title; ?></h2>

          <?php echo $text; ?>

          <a href="<?php echo $link; ?>" class="singlelink"><?php echo $button; ?> <i class="fas fa-caret-right"></i></a>

        </div>

      <?php endwhile; ?>
      <?php endif; ?>
      
      <div class="clear"></div>

    </div>

  </section>

  <section class="atuacaohome">
  
    <div class="content">

      <?php if( have_rows('atuacao_section') ): ?>
      <?php while( have_rows('atuacao_section') ): the_row(); 

          $title = get_sub_field('titulo_atuacao');
          $text = get_sub_field('texto_atuacao');
          $image = get_sub_field('imagem_atuacao');

      ?>

        <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>" />
    
        <div class="text">
        
          <h2 class="title"><?php echo $title; ?></h2>

          <?php echo $text; ?>

        </div>

      <?php endwhile; ?>
      <?php endif; ?>
      
      <div class="clear"></div>

    </div>

  </section>

  <?php endwhile; ?>

  <?php wp_reset_postdata(); ?>

  <?php else : ?>
    <p><?php _e( 'Desculpe, nenhum serviço encontrado.' ); ?></p>
  <?php endif; ?>

  <section class="homepub">
  
    <div class="content">
    
      <h2 class="sectitle"><?php 
          if(pll_current_language() == 'es') {
              echo 'Publicaciones';
          } else if(pll_current_language() == 'pt') {
              echo 'Veja nossas publicações'; 
          } else if (pll_current_language() == 'en') {
            echo 'Publications';
          }
      ?></h2>

      <div class="homeblog">
      
        <?php

        $postsdestaque_query_args = array(
          'post_type' => 'library', 
          'posts_per_page' => 3,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date',
        );

        $postsdestaque_query = new WP_Query( $postsdestaque_query_args );

        if ( $postsdestaque_query->have_posts() ) : ?>

        <?php while( $postsdestaque_query->have_posts() ) : $postsdestaque_query->the_post(); ?>

          <?php get_template_part( 'components/library-card' ) ?>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum post nessa seção.').'</p>'; endif; ?>

      </div>

    </div>

  </section>

  <section class="partners">
  
    <div class="content">

    <?php 
      $args = array(
        'pagename' => 'home', 
      );
      $the_query = new WP_Query( $args ); 
    ?>
    <?php if ( $the_query->have_posts() ) : ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    
      <h2 class="sectitle"><?php the_field('titulo_secao_membros'); ?></h2>

      <p class="secsubtitle"><?php the_field('texto_secao_membros'); ?></p>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

    <?php else : ?>
      <p><?php _e( 'Desculpe, nenhum serviço encontrado.' ); ?></p>
    <?php endif; ?>

      <div class="partnerlist">

        <?php

        $partner_query_args = array(
            'post_type' => 'partner', 
            'posts_per_page' => -1,
            'post_status' => 'publish',
            // alphabetical order
            'order' => 'ASC',
            'orderby' => 'title',
            'tax_query' => array(
                'relation' => 'AND',
                [
                    'taxonomy' => 'category_partner',
                    'field'    => 'term_id',
                    'terms'    => [37],
                    'operator' => 'NOT IN',
                ]
            ),
        );

        $partner_query = new WP_Query( $partner_query_args );

        if ( $partner_query->have_posts() ) : ?>

        <?php while( $partner_query->have_posts() ) : $partner_query->the_post(); ?>

          <?php get_template_part( 'components/partner-card' ) ?>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum parceiro cadastrado em nosso site.').'</p>'; endif; ?>

      </div>

      <a href="<?php bloginfo('url'); ?>/quem-somos" class="btn center green"><?php 
          if(pll_current_language() == 'es') {
              echo 'Lee más';
          } else if(pll_current_language() == 'pt') {
              echo 'Todos os parceiros'; 
          } else {
            echo 'Read more';
          }
      ?></a>

    </div>

  </section>

  <?php get_template_part( 'components/newsletter' ) ?>

</div>

<?php get_footer(); ?>