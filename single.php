<?php get_header() ?>

<div class="internas content">

<div id="conteudo"></div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="singlepost">

          <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

          <h1><?php the_title(); ?></h1>

          <p class="data"><i class="far fa-calendar-alt"></i> <?php echo get_the_date(); ?> / <?php $c = get_the_category(); echo $c[0]->cat_name; ?></p>

          <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>" class="destacada">

          <?php the_content(); ?>

          <div class="clear"></div>

    </div>

    <div class="sideblog">

      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Blog") ) : ?><?php endif;?>

    </div>

    <?php endwhile; else: ?>
      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

  <div class="clear"></div>

</div>

<?php get_footer() ?>