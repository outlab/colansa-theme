<?php get_header() ?>

<div class="internas content">

<div id="conteudo"></div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="singlelibrary">

          <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

          <h1 class="pagetitle"><?php the_title(); ?></h1>

          <p class="cat"><?php
            $categories = get_the_terms( $post->ID , 'category_library' );
            $i = 1;
            foreach ( $categories as $category ) {
              echo $category->name;
              echo ($i < count($categories))? " / " : "";
              $i++;
            }
          ?> | <?php
          $categories = get_the_terms( $post->ID , 'type_library' );
          $i = 1;
          foreach ( $categories as $category ) {
            echo $category->name;
            echo ($i < count($categories))? " / " : "";
            $i++;
          } 
          ?></p>

          <div class="text">
            <?php the_content(); ?>
          </div>

          <div class="clear"></div>

    </div>

    <?php endwhile; else: ?>
      <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>

  <div class="clear"></div>

  <?php get_template_part( 'components/newsletter' ) ?>

</div>

<?php get_footer() ?>