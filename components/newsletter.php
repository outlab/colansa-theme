<section class="newsletter">

  <div class="content">

  <?php 
      $args = array(
        'pagename' => 'home', 
      );
      $the_query = new WP_Query( $args ); 
    ?>
    <?php if ( $the_query->have_posts() ) : ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    
      <h2 class="sectitle"><?php the_field('titulo_secao_newsletter'); ?></h2>

      <p class="secsubtitle"><?php the_field('texto_secao_newsletter'); ?></p>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

    <?php else : ?>
      <p><?php _e( 'Desculpe, nenhum serviço encontrado.' ); ?></p>
    <?php endif; ?>

    <!-- Begin Mailchimp Signup Form -->
    <div id="mc_embed_signup">
    <form action="https://colansa.us1.list-manage.com/subscribe/post?u=cdeb1e74cbae2e0cb453df236&amp;id=fef3224d21" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="nombre@email.com">
      <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cdeb1e74cbae2e0cb453df236_fef3224d21" tabindex="-1" value=""></div>
      <input type="submit" value="REGISTRO" name="subscribe" id="mc-embedded-subscribe" class="button">
    </form>
    </div>
    <!--End mc_embed_signup-->

    <!-- Formulário original (sem ação) -->
    <!--
    <form action="#" method="post">

      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="seu@email.com">

      <input type="submit" value="REGISTRO" class="button">
      </div>
    </form>
    -->
    <!-- Formulário original (sem ação) -->

  </div>

</section>