<div class="eventcard">

  <a href="<?php the_permalink(); ?>"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>" alt="<?php $img_id = get_post_thumbnail_id(get_the_ID()); echo get_post_meta( $img_id, '_wp_attachment_image_alt', true ); ?>"></a>

  <p class="category">
    <?php
      $categories = get_the_terms( $post->ID , 'category_events' );
      $i = 1;
      foreach ( $categories as $category ) {
        echo $category->name;
        echo ($i < count($categories))? " / " : "";
        $i++;
      }
    ?>
  </p>

  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

  <p class="data"><?php the_field('tipo_evento'); ?> | <?php the_field('data_evento'); ?> - <?php the_field('data_evento_final'); ?></p>

  <a href="<?php the_permalink(); ?>" class="btn center green"><?php 
          if(pll_current_language() == 'es') {
              echo 'Sepa más';
          } else if(pll_current_language() == 'pt') {
              echo 'Saiba Mais'; 
          }  
      ?></a>

  <div class="clear"></div>

</div>