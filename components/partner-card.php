<div class="partnercard">

  <a href="<?php the_field('link_membro'); ?>" target="_blank"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>" alt="<?php $img_id = get_post_thumbnail_id(get_the_ID()); echo get_post_meta( $img_id, '_wp_attachment_image_alt', true ); ?>"></a>

  <?php the_content(''); ?>

  <div class="clear"></div>

</div>