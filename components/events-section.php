<section id="forms-events">

    <form>

        <input class="input-library" type="text" name="textevent" placeholder="Pesquisar eventos">

        <select class="select-library" name="catevent" id="">
            <option value="">Categoria</option>
            <?php $types = get_categories('taxonomy=category_events&post_type=events&hide_empty=1'); 
              foreach ($types as $type) : 
              echo '<option value="'.$type->term_id.'"'; 
              echo '>'.$type->name.'</option>'; 
            endforeach; 
            ?> 
        </select>

        <button class="btn-form"><?php 
          if(pll_current_language() == 'es') {
              echo 'Buscar';
          } else if(pll_current_language() == 'pt') {
              echo 'Pesquisar'; 
          }  
      ?></button>

    </form>

</section>

<div class="eventslist">
      
    <?php

      if( '' == $_GET['catevent'] && '' != $_GET['textevent']) {

        $search_term = $_GET['textevent'];
        
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'events', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date', 
          's' => $search_term,
        )); 

      }

      else if( '' != $_GET['catevent'] && '' == $_GET['textevent']) {
        $category = $_GET['catevent'];
        
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'events', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date', 
          'tax_query' => array(
            array( 
              'taxonomy' => 'category_events', 
              'field' => 'id', 
              'terms' => $category 
              )  
            ) 
        )); 

      }
      
      else {
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'events', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date',
        )); 

      }

    if ( $postsdestaque_query->have_posts() ) : ?>

    <?php while( $postsdestaque_query->have_posts() ) : $postsdestaque_query->the_post(); ?>

      <?php get_template_part( 'components/event-card' ) ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, nenhum evento encontrado').'</p>'; endif; ?>

  </div>