<div class="librarycard">

  <a href="<?php the_field('link_arquivo'); ?>" target="_blank"><img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); echo $image[0]; ?>" alt="<?php $img_id = get_post_thumbnail_id(get_the_ID()); echo get_post_meta( $img_id, '_wp_attachment_image_alt', true ); ?>"></a>

  <p class="category">
    <?php
      $categories = get_the_terms( $post->ID , 'category_library' );
      $i = 1;
      foreach ( $categories as $category ) {
        echo $category->name;
        echo ($i < count($categories))? " / " : "";
        $i++;
      }
    ?>
  </p>

  <h2><a href="<?php the_field('link_arquivo'); ?>" target="_blank"><?php the_title(); ?></a></h2>

  <a href="<?php the_field('link_arquivo'); ?>" target="_blank" class="singlelink">
      <?php 
          if(pll_current_language() == 'es') {
              echo 'Lee más';
          } else if(pll_current_language() == 'pt') {
              echo 'Leia mais'; 
          } else if (pll_current_language() == 'en') {
            echo 'Read more';
          }
      ?>
       <i class="fas fa-caret-right"></i></a>

  <div class="clear"></div>

</div>