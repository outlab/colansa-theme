<section id="forms-library">

    <form>

        <input class="input-library" name="postname" type="text" placeholder="<?php 
          if(pll_current_language() == 'es') {
              echo 'Buscar publicaciones';
          } else if(pll_current_language() == 'pt') {
              echo 'Pesquisar publicações'; 
          } else if (pll_current_language() == 'en') {
              echo 'Search publications';
          }
        ?>">

        <!-- <select class="select-library" name="typelib">
            <option value="">Tipo</option>
            <?php $types = get_categories('taxonomy=type_library&post_type=library&hide_empty=1'); 
              foreach ($types as $type) : 
              echo '<option value="'.$type->term_id.'"'; 
              echo '>'.$type->name.'</option>'; 
            endforeach; 
            ?> 
        </select> -->

        <select class="select-library" name="catlib">
            <option value=""><?php 
            if (pll_current_language() == 'es') {
                echo 'Categoría';
            } else if (pll_current_language() == 'pt') {
                echo 'Categoria'; 
            } else if (pll_current_language() == 'en') {
                echo 'Category';
            } 
            ?></option>
            <?php $cats = get_categories('taxonomy=category_library&post_type=library&hide_empty=1'); 
              foreach ($cats as $cat) : 
              echo '<option value="'.$cat->term_id.'"'; 
              echo '>'.$cat->name.'</option>'; 
            endforeach; 
            ?> 
        </select>

        <button class="btn-form"><?php 
            if (pll_current_language() == 'es') {
                echo 'Buscar';
            } else if(pll_current_language() == 'pt') {
                echo 'Pesquisar'; 
            } else if (pll_current_language() == 'en') {
                echo 'Search';
            }
            ?></button>

    </form>

</section>

<div class="homeblog">
      
    <?php

      if( '' == $_GET['catlib'] && '' != $_GET['postname']) {

        $search_term = $_GET['postname'];
        
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'library', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date', 
          's' => $search_term,
        )); 

      }

      else if( '' != $_GET['catlib'] && '' == $_GET['postname']) {
        $category = $_GET['catlib'];
        
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'library', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date', 
          'tax_query' => array(
            array( 
              'taxonomy' => 'category_library', 
              'field' => 'id', 
              'terms' => $category 
              )  
            ) 
        )); 

      }

      else {
        $postsdestaque_query = new WP_Query( array( 
          'post_type' => 'library', 
          'posts_per_page' => -1,
          'post_status' => 'publish',
          'order' => 'DESC',
          'orderby' => 'date',
        )); 

      }

    if ( $postsdestaque_query->have_posts() ) : ?>

    <?php while( $postsdestaque_query->have_posts() ) : $postsdestaque_query->the_post(); ?>

      <?php get_template_part( 'components/library-card' ) ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, nenhum arquivo encontrado na biblioteca.').'</p>'; endif; ?>

  </div>