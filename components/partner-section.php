<section class="partnersinner">
    
    <div class="content">
    
        <h2 class="sectitle"><?php 
          if(pll_current_language() == 'es') {
              echo 'Nuestros miembros';
          } else if(pll_current_language() == 'pt') {
              echo 'Conheça nossos parceiros'; 
          }  
      ?></h2>

        <div class="partnerlist">

        <?php

        $partner_query_args = array(
            'post_type' => 'partner', 
            'posts_per_page' => -1,
            'post_status' => 'publish',
            // alphabetical order
            'order' => 'ASC',
            'orderby' => 'title',
            'tax_query' => array(
                'relation' => 'AND',
                [
                    'taxonomy' => 'category_partner',
                    'field'    => 'term_id',
                    'terms'    => [37],
                    'operator' => 'NOT IN',
                ]
            ),
        );

        $partner_query = new WP_Query( $partner_query_args );

        if ( $partner_query->have_posts() ) : ?>

        <?php while( $partner_query->have_posts() ) : $partner_query->the_post(); ?>

            <?php get_template_part( 'components/partner-card' ) ?>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum parceiro cadastrado em nosso site.').'</p>'; endif; ?>

        </div>

        <h2 class="sectitle"><?php 
          if(pll_current_language() == 'es') {
              echo 'Apoyo';
          } else if(pll_current_language() == 'pt') {
              echo 'Apoio'; 
          }  
        ?></h2>

        <?php

        $partner_query_args = array(
            'post_type' => 'partner', 
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'order' => 'DESC',
            'orderby' => 'date',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'category_partner',
                    'field'    => 'term_id',
                    'terms'    => [37],
                    'operator' => 'IN',
                )
            ),
        );

        $partner_query = new WP_Query( $partner_query_args );

        if ( $partner_query->have_posts() ) : ?>

        <?php while( $partner_query->have_posts() ) : $partner_query->the_post(); ?>

            <?php get_template_part( 'components/partner-card' ) ?>

        <?php endwhile; ?>

        <?php wp_reset_postdata(); else: echo '<p>'.__('Desculpe, ainda não possuímos nenhum parceiro cadastrado em nosso site.').'</p>'; endif; ?>

    </div>

</section>