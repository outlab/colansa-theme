/********** JQUERY *****************/

$(document).ready(function() {

    $(window).on('load', function() {
        $("#cover").hide();
    });
    setTimeout(function(){$('#cover').hide();}, 500);

    // 

    $(".openinsc").click(function() {
        $(".insc").toggle("fast");
    });

    // ÂNCORAS

    $('a[href*="#"]')
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top - 150
            }, 1000, function() {
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                return false;
              } else {
                $target.attr('tabindex','-1');
                $target.focus();
              };
            });
          }
        }
      });

});

/************ JS *******************/


function openMenu() {
    var element = document.getElementById("menu-menu-principal");
    var menumobile = document.getElementById("menumobile");
    var closemenumobile = document.getElementById("closemenumobile");
  
    if (element.style.display === "block") {
      element.style.display = "none";
      menumobile.style.display = "block";
      closemenumobile.style.display = "none";
    } else {
      element.style.display = "block";
      menumobile.style.display = "none";
      closemenumobile.style.display = "block";
    }
  }
  
  function closeMenu() {
    var element = document.getElementById("menu-menu-principal");
    var menumobile = document.getElementById("menumobile");
    var closemenumobile = document.getElementById("closemenumobile");
  
    if (element.style.display === "block") {
      element.style.display = "none";
      menumobile.style.display = "block";
      closemenumobile.style.display = "none";
    } else {
      element.style.display = "block";
      menumobile.style.display = "none";
      closemenumobile.style.display = "block";
    }
  }

  /************ ACESSIBILIDADE *******************/

  var contraste = 1;

  $('.contraste').on('click',function(){
      if ( contraste == 1 ) {
          $('body').addClass('constrasteon');
          contraste = 2;
          Cookies.set('contraste', 'ativo');
      } else if ( contraste == 2) {
          $('body').removeClass('constrasteon');
          contraste = 1;
          Cookies.remove('contraste');
      } else {
          $('body').removeClass('constrasteon');
          contraste = 1;
          Cookies.remove('contraste');
      }
  });

  if ( Cookies.get('contraste') === 'ativo' ) {
      $('body').addClass('constrasteon');
  }

  var aumentar = 1;

  $('.aumentar').on('click',function(){
      if ( aumentar == 1 ) {
          $('body').addClass('aumentar');
          aumentar = 2;
          Cookies.set('aumentar', 'ativo');
      } else if ( aumentar == 2) {
          $('body').removeClass('aumentar');
          aumentar = 1;
          Cookies.remove('aumentar');
      } else {
          $('body').removeClass('aumentar');
          aumentar = 1;
          Cookies.remove('aumentar');
      }
  });

  if ( Cookies.get('aumentar') === 'ativo' ) {
      $('body').addClass('aumentar');
  }
