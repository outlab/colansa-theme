<?php

// 1. STYLES

function colansa_register_styles() {

  // Add main CSS
  wp_enqueue_style( 'montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,800;1,400;1,800&display=swap', null, null, 'all' );

  // Add main CSS
  wp_enqueue_style(
    'main',
    get_template_directory_uri() . '/assets/css/main.css',
    null,
    '1.2.1',
    'screen'
  );
  
  // Add mobile CSS
	wp_enqueue_style(
    'mobile',
    get_template_directory_uri() . '/assets/css/mobile.css',
    null,
    '1.0.0',
    'screen'
  );
  
}

add_action( 'wp_enqueue_scripts', 'colansa_register_styles' );

// 2. SCRIPTS

function colansa_register_scripts() {

  wp_enqueue_script( 'jq', get_template_directory_uri() . '/assets/js/jquery-3.2.1.min.js', array(), null, true );
  wp_script_add_data( 'jq', 'async', true );
  
  wp_enqueue_script( 'phoenix', get_template_directory_uri() . '/assets/js/jquery.phoenix.min.js', array(), null, true );
  wp_script_add_data( 'phoenix', 'async', true );
  
  wp_enqueue_script( 'less', get_template_directory_uri() . '/assets/js/less.min.js', array(), null, true );
  wp_script_add_data( 'less', 'async', true );
  
  wp_enqueue_script( 'cookie', 'https://cdn.jsdelivr.net/npm/js-cookie@beta/dist/js.cookie.min.js', array(), null, true );
  wp_script_add_data( 'cookie', 'async', true );
  
  wp_enqueue_script( 'mask', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js', array(), null, true );
  wp_script_add_data( 'mask', 'async', true );
  
  wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), null, true );
	wp_script_add_data( 'main', 'async', true );

}

add_action( 'wp_enqueue_scripts', 'colansa_register_scripts' );

// 3. CUSTOM POSTS

include_once ( get_template_directory() . "/functions/slider-cpt.php" );
include_once ( get_template_directory() . "/functions/partner-cpt.php" );
include_once ( get_template_directory() . "/functions/library-cpt.php" );
include_once ( get_template_directory() . "/functions/events-cpt.php" );

// 4. MISC

show_admin_bar(false);

add_theme_support( 'title-tag' );
add_theme_support( 'align-wide' );
add_theme_support( 'responsive-embeds' );

add_theme_support( 'post-thumbnails' );

function register_my_menu() {
  register_nav_menus(
      array(
        'header-menu' => __( 'Header Menu' ),
        'footer-menu' => __( 'Footer Menu' ),
      )
    );
  }
add_action( 'init', 'register_my_menu' );

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

// SIDEBAR

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Sidebar Blog',
    'id' => 'blog',
    'before_widget' => '<div class="widget widgetizedArea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);

// BREADCRUMB

function get_breadcrumb() {
  echo '<a href="'.home_url().'" rel="nofollow"><i class="fas fa-home"></i></a>';
   if (is_category()) {
       echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
       echo '<a href="'.home_url().'/blog" rel="nofollow">Blog</a>';
       echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
       echo '<strong>';
       the_category(' &bull; ');
       echo '</strong>';
   }
   elseif (is_page()) {
       echo " &nbsp;&nbsp;/&nbsp;&nbsp; ";
       echo '<strong>';
       echo the_title();
       echo '</strong>';
   }
   elseif (is_404()) {
    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
     echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
     echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
     echo '<strong>';
     echo '404';
     echo '</strong>';
}
   elseif (is_search()) {
       echo "&nbsp;&nbsp;/&nbsp;&nbsp;Resultados de busca para ... ";
       echo '"<em>';
       echo the_search_query();
       echo '</em>"';
   }
   elseif ( is_singular( 'post' ) ) {
     echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
     echo '<a href="'.home_url().'/blog" rel="nofollow">Blog</a>';
     echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
     echo '<strong>';
     echo the_title();
     echo '</strong>';
  }
  elseif ( is_singular( 'events' ) ) {
    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
    echo '<a href="'.home_url().'/eventos" rel="nofollow">Eventos</a>';
    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
    echo '<strong>';
    echo the_title();
    echo '</strong>';
  }
  elseif ( is_singular( 'library' ) ) {
    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
    echo '<a href="'.home_url().'/biblioteca" rel="nofollow">Biblioteca</a>';
    echo "&nbsp;&nbsp;/&nbsp;&nbsp;";
    echo '<strong>';
    echo the_title();
    echo '</strong>';
  }
    echo '';
  }

   //

   function polylang_shortcode() {
    ob_start();
    pll_the_languages(array('show_flags'=>0,'show_names'=>1,'display_names_as'=>'slug'));
    $flags = ob_get_clean();
    return $flags;
    }
    add_shortcode( 'polylang', 'polylang_shortcode' );

?>