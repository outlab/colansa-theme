<!DOCTYPE html>

<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_directory');?>/favicon.png" />
  
    <script type="text/javascript">
        var templateUrl = '<?= get_bloginfo("url"); ?>';
        var userID = '<?php global $current_user; echo $current_user->ID ?>';
        var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
    </script>

    <link rel="stylesheet" href="https://use.typekit.net/ehy2ovf.css">

  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <div id="cover"></div>

    <section class="preheader">
    
        <div class="content">

           <div class="idioma">
              <?php echo do_shortcode('[polylang]'); ?>
            </div>
        
            <div class="nav">

                <a href="#conteudo"><p><?php 
                if(pll_current_language() == 'es') {
                    echo 'Ir al contenido';
                } else if(pll_current_language() == 'pt') {
                    echo 'Ir para conteúdo'; 
                } else if (pll_current_language() == 'en') {
                    echo 'Go to content';
                }?> <span>1</span></p></a>

                <a href="#menu"><p><?php 
                if(pll_current_language() == 'es') {
                    echo 'Ir al menú';
                } else if(pll_current_language() == 'pt') {
                    echo 'Ir para o menu'; 
                } else if (pll_current_language() == 'en') {
                    echo 'Go to menu';
                }?> <span>2</span></p></a>

                <a href="#footer"><p><?php 
                if(pll_current_language() == 'es') {
                    echo 'Ir al pie de página';
                } else if(pll_current_language() == 'pt') {
                    echo 'Ir para o rodapé'; 
                } else if (pll_current_language() == 'en') {
                    echo 'Go to footer';
                }?> <span>3</span></p></a>
                

            </div>

            <div class="redes">

              <a href="https://twitter.com/ColansaLAC" target="_blank"><i class="fab fa-twitter"></i></a>

              <a href="https://www.youtube.com/channel/UCJIgx7-bRRUTrgGavtesw6w" target="_blank"><i class="fab fa-youtube"></i></a>

            </div>

            <div class="access">
                <p><span class="contraste">AUTOCONTRASTE</span> <i class="fas fa-adjust contraste"></i> | <i class="fas fa-text-height aumentar"></i></p>
            </div>
        </div>

    </section>

    <header id="menu">        

        <div class="content">
        
            <p class="logo <?php 
                if(pll_current_language() == 'es') {
                    echo 'logoes';
                } else if(pll_current_language() == 'pt') {
                    echo 'logobr'; 
                } else {
                    echo 'logoes';
                }?>">
                <a href="<?php bloginfo('url'); ?>">Colansa</a>
            </p>

            <div id="menumobile"><i class="fas fa-bars menumobile" onclick="openMenu()"></i></div>
            <div id="closemenumobile"><i class="fas fa-times closemenumobile" onclick="closeMenu()"></i></div>

            <div class="idioma">
              <?php echo do_shortcode('[polylang]'); ?>
            </div>
            
            <?php wp_nav_menu( array(
                'theme_location' => 'header-menu',
                'menu_id' => 'menu-menu-principal'
            ) ); ?>           

        </div>

        <div class="clear"></div>

    </header>